signature ACL_OPERATORS =
sig
    datatype t
      = IF
      | ATOM of string
      | ATOMP
      | CAR
      | CDR
      | CONS
      | EQUAL
      | SIZE
      | FUNCTION of {name : string, arity : int}
      | NAT of int
      | NATP
      | PLUS
      | LT

    val eq : t * t -> bool
    val arity : t -> Arity.t
    val toString : t -> string

    val nilOp : t
    val tOp : t
end
