functor Check(D : DEF) : CHECK where type Def.t = D.t =
struct
  structure Def = D
  open Def

  type context = t list
  datatype result = MALFORMED | STUCK of Def.Term.t | SUCCEEDED

  fun lookupFun cxt {name = name', arity = arity'} =
    case List.find (fn FUN {name, formals, ...} =>
                       name = name' andalso List.length formals = arity'
                     | _ => false)
                   cxt
     of
        SOME (THM info) => SOME info
      | _ => NONE

  fun lookupThm cxt {name = name', arity = arity'} =
    case List.find (fn THM {name, formals, ...} =>
                       name = name' andalso List.length formals = arity'
                     | AX {name, formals, ...} =>
                       name = name' andalso List.length formals = arity'
                     |  _ => false)
                   cxt of
        SOME (AX info) => SOME info
      | SOME (THM {name, formals, claim, ...}) =>
        SOME {name = name, formals = formals, claim = claim}
      | _ => NONE

  fun existsFun cxt = Option.isSome o lookupFun cxt
  fun existsObj cxt args =
    Option.isSome (lookupThm cxt args) orelse existsFun cxt args

  fun wfTerm cxt freeVars t =
    let
      open Term
      open AclOperators

      fun wfFun {name, arity} =
        Option.isSome
          (List.find (fn FUN {name = name', formals, ...} =>
                         name = name' andalso List.length formals = arity
                     | _ => false)
                     cxt)
    in
      List.all (fn v => List.exists (fn v' => Variable.eq (v, v')) freeVars)
               (freeVariables t)
      andalso List.all (fn (FUNCTION args) => wfFun args | _ => true )
                       (Term.operators t)
    end

  fun wfStep cxt freeVars t =
    let
      open Term
      infix $
      open AclOperators

      fun app IF = false
        | app (ATOM _) = false
        | app (NAT _) = false
        | app _ = true
    in
      case out t of
          FUNCTION args $ subterms =>
          existsObj cxt args andalso Vector.all (wfTerm cxt freeVars) subterms
        | oper $ args => app oper andalso Vector.all (wfTerm cxt freeVars) args
        | _ => false
    end

  fun check cxt D = raise Fail ""
end
