signature DEF =
sig
    structure Term : ABT_UTIL where type Operator.t = AclOperators.t

    datatype focus
      = HERE
      | THERE_AP of (int * focus)
      | THERE_Q of focus
      | THERE_A of focus
      | THERE_E of focus

    datatype proof = PROOF of (focus * Term.t) list

    datatype t
      = FUN of { name     : string
               , formals  : Term.Variable.t list
               , body     : Term.t
               , seed     : Term.t option
               , totality : proof list}
      | THM of { name    : string
               , formals : Term.Variable.t list
               , claim   : Term.t
               , seed    : Term.t option
               , proof   : proof}
      | AX of { name    : string
              , formals : Term.Variable.t list
              , claim   : Term.t}

    exception NoSuchFocus
    val view    : focus -> Term.t -> Term.t
    val rewrite : focus -> {old : Term.t, new : Term.t} -> Term.t

    val apply : t -> Term.t list -> Term.t
end
