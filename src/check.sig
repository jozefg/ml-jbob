signature CHECK =
sig
    structure Def : DEF
    type context = Def.t list

    datatype result = MALFORMED | STUCK of Def.Term.t | SUCCEEDED
    val check : context -> Def.t -> result
end
