structure AclOperators :> ACL_OPERATORS =
struct
  datatype t
    = IF
    | ATOM of string
    | ATOMP
    | CAR
    | CDR
    | CONS
    | EQUAL
    | SIZE
    | FUNCTION of {name : string, arity : int}
    | NAT of int
    | NATP
    | PLUS
    | LT

  val eq = op=

  fun arity IF = #[0, 0, 0]
    | arity (ATOM _) = #[]
    | arity ATOMP = #[0]
    | arity CAR   = #[0]
    | arity CDR   = #[0]
    | arity CONS  = #[0, 0]
    | arity EQUAL = #[0, 0]
    | arity SIZE  = #[0]
    | arity (FUNCTION {arity, ...}) = Vector.tabulate (arity, fn _ => 0)
    | arity (NAT _) = #[]
    | arity NATP = #[0]
    | arity PLUS = #[0, 0]
    | arity LT = #[0, 0]

  fun toString IF = "if"
    | toString (ATOM s) = "'" ^ s
    | toString ATOMP = "atom"
    | toString CAR = "car"
    | toString CDR = "cdr"
    | toString CONS = "cons"
    | toString EQUAL = "cons"
    | toString SIZE = "size"
    | toString (FUNCTION {name, ...}) = name
    | toString (NAT i) = Int.toString i
    | toString NATP = "natp"
    | toString PLUS = "+"
    | toString LT = "<"

  val nilOp = ATOM "nil"
  val tOp = ATOM "t"
end

structure Acl = AbtUtil(Abt(structure Variable = Variable ()
                            structure Operator = AclOperators))
