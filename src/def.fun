functor Def(A : ABT_UTIL
              where type Operator.t = AclOperators.t) :> DEF =
struct
  structure Term = A
  open Term
  open AclOperators

  infix $ \ $$ \\

  datatype focus
    = HERE
    | THERE_AP of (int * focus)
    | THERE_Q of focus
    | THERE_A of focus
    | THERE_E of focus

    datatype proof = PROOF of (focus * Term.t) list

    datatype t
      = FUN of { name     : string
               , formals  : Term.Variable.t list
               , body     : Term.t
               , seed     : Term.t option
               , totality : proof list}
      | THM of { name    : string
               , formals : Term.Variable.t list
               , claim   : Term.t
               , seed    : Term.t option
               , proof   : proof}
      | AX of { name    : string
              , formals : Term.Variable.t list
              , claim   : Term.t}


  fun apply (FUN {body, ...}) terms = List.foldl Term.// body terms
    | apply (THM {claim, ...}) terms = List.foldl Term.// claim terms
    | apply (AX {claim, ...}) terms = List.foldl Term.// claim terms

  exception NoSuchFocus

  fun view HERE t = t
    | view (THERE_Q rest) t = (
        case out t of
            IF $ #[i, t, e] => view rest i
          | _ => raise NoSuchFocus
    )
    | view (THERE_A rest) t = (
        case out t of
            IF $ #[i, t, e] => view rest t
          | _ => raise NoSuchFocus
    )
    | view (THERE_E rest) t = (
        case out t of
            IF $ #[i, t, e] => view rest e
          | _ => raise NoSuchFocus
    )
    | view (THERE_AP (i, rest)) t =
      let
        val (oper, args) =
          case out t of
              oper $ args => (oper, args)
            | _ => raise NoSuchFocus
        val () =
          case oper of
              IF => raise NoSuchFocus
            | ATOM _ => raise NoSuchFocus
            | NAT _ => raise NoSuchFocus
            | _ => ()
      in
        view rest (Vector.sub (args, i))
        handle Subscript => raise NoSuchFocus
      end

  fun rewrite HERE {old, new} = new
    | rewrite (THERE_Q rest) {old, new} = (
        case out old of
            IF $ #[i, t, e] => IF $$ #[rewrite rest {old = i, new = new}, t, e]
          | _ => raise NoSuchFocus
    )
    | rewrite (THERE_A rest) {old, new} = (
        case out old of
            IF $ #[i, t, e] => IF $$ #[i, rewrite rest {old = t, new = new}, e]
          | _ => raise NoSuchFocus
    )
    | rewrite (THERE_E rest) {old, new} = (
        case out old of
            IF $ #[i, t, e] => IF $$ #[i, t, rewrite rest {old = e, new = new}]
          | _ => raise NoSuchFocus
    )
    | rewrite (THERE_AP (i, rest)) {old, new} =
      let
        val (oper, args) =
          case out old of
              oper $ args => (oper, args)
            | _ => raise NoSuchFocus
        val () =
          case oper of
              IF => raise NoSuchFocus
            | ATOM _ => raise NoSuchFocus
            | NAT _ => raise NoSuchFocus
            | _ => ()

        val args' =
          Vector.update (args, i, rewrite rest {old = Vector.sub (args, i), new = new})
          handle Subscript => raise NoSuchFocus
      in
        oper $$ args'
      end
end
